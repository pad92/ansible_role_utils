# utils

Install default package on redhat and debian family
Set motd and issue

## Requirements

none

## Role Variables

### system upgrade

```
cts_role_utils_upgrade:
  upgrade: no
```

### name into issue and motd

```
cts_role_utils_compagnie:
  name: CLOUD TEMPLE
  bu: Services
```

## Dependencies

None

## Example Playbook

```
- hosts: all
  gather_facts: False
  pre_tasks:
    - name: pre_tasks | setup python
      raw: command -v yum >/dev/null && yum -y install python python-simplejson libselinux-python redhat-lsb || true ; command -v apt-get >/dev/null && sed -i '/cdrom/d' /etc/apt/sources.list && apt-get update && apt-get install -y python python-simplejson lsb-release aptitude || true
      changed_when: False
    - name: pre_tasks | gets facts
      setup:
      tags:
      - always

  roles:
    - utils
```

